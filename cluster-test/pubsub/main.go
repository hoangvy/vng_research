package main

import (
	"time"

	"gitlab.com/hoangvy/vng_research/cluster-test/pubsub/rediscli"
)

type valueEx struct {
	Name  string
	Email string
}

func main() {
	//Use your actually ip address here
	value := "aloha"
	channel := "subscribe"
	time.Sleep(1000 * time.Millisecond)
	rediscli.Publish(channel, value)
}
