package rediscli

import (
	"fmt"

	"github.com/go-redis/redis"
)

var (
	client = &redisCluterClient{}
)

//RedisClusterClient struct
type redisCluterClient struct {
	c  *redis.ClusterClient
	in chan<- string
}

//GetClient get the redis client
func init() {
	addr := []string{"127.0.0.1:30001", "127.0.0.1:30002", "127.0.0.1:30003"}
	conn := redis.NewClusterClient(&redis.ClusterOptions{
		Addrs: addr,
	})

	if err := conn.Ping().Err(); err != nil {
		panic("Unable to connect to redis " + err.Error())
	}
	client.c = conn
	go Subscribe("subscribe")
}

//GetKey get key
func Subscribe(channel string) {
	pubsub := client.c.Subscribe(channel)

	// Wait for confirmation that subscription is created before publishing anything.
	_, err := pubsub.Receive()
	if err != nil {
		panic(err)
	}

	ch := pubsub.Channel()

	for msg := range ch {
		fmt.Println(msg.Channel, msg.Payload)
	}
}

func Publish(channel, msg string) {
	err := client.c.Publish(channel, msg).Err()
	if err != nil {
		panic(err)
	}
}
