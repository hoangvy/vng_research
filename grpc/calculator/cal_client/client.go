package main

import (
	"context"
	"fmt"
	"io"
	"log"
	"time"

	"gitlab.com/hoangvy/vng_research/grpc/calculator/calculatorpb"
	"google.golang.org/grpc"
	"google.golang.org/grpc/status"
)

func main() {
	conn, err := grpc.Dial("localhost:50052", grpc.WithInsecure())
	if err != nil {
		log.Fatal("Can not connect to port, %v", err)
	}

	defer conn.Close()

	c := calculatorpb.NewCalculateServiceClient(conn)

	// doUnary(c)
	// doServerStream(c)
	doClientStream(c)
	// doBidiStream(c)
}

func doUnary(c calculatorpb.CalculateServiceClient) {
	fmt.Println("Starting Urany RPC...")
	req := &calculatorpb.CalRequest{
		Arg1: 10,
		Arg2: 3,
	}

	res, err := c.SumTwoNumber(context.Background(), req)
	if err != nil {
		log.Fatalf("error when calling Sum: %v", err)
	}
	log.Printf("Result from server: %v", res.GetRes())
}

func doServerStream(c calculatorpb.CalculateServiceClient) {
	fmt.Println("Starting Server Streaming RPC...")
	req := &calculatorpb.PrimeNumberRequest{
		Input: 210,
	}

	streamRes, err := c.PrimeNumberDecomposition(context.Background(), req)
	if err != nil {
		log.Fatalf("error when calling Prime: %v", err)
	}

	for {
		msg, err := streamRes.Recv()
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Fatalf("error when reading stream: %v", err)
		}
		fmt.Printf("Prime return from server: %v \n ", msg.GetPrime())
	}
}

func doClientStream(c calculatorpb.CalculateServiceClient) {
	fmt.Println("Starting Client Streaming RPC...")

	reqs := []int64{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}

	stream, err := c.ComputeAverage(context.Background())
	if err != nil {
		log.Fatalf("Error when calling compute average: %v", err)
	}

	for _, req := range reqs {
		fmt.Printf("Sending to server: %v\n", req)
		sendReq := &calculatorpb.ComputeAverageRequest{
			Element: req,
		}
		stream.Send(sendReq)
		time.Sleep(1000 * time.Millisecond)
	}
	time.Sleep(60 * time.Second)
	res, err := stream.CloseAndRecv()
	if err != nil {
		log.Fatalf("error while receiving response from server: %v", err)
	}

	fmt.Printf("LongGreet response: %v\n", res.GetResult())

}

func doBidiStream(c calculatorpb.CalculateServiceClient) {
	fmt.Println("Start Bidirection Streaming RPC...")

	stream, err := c.FindMaximum(context.Background())
	if err != nil {
		log.Fatalf("error when send stream: %v\n", err)
	}

	reqs := []int64{1, 5, 3, 6, 2, 20}

	waitc := make(chan struct{})
	go func() {
		for _, req := range reqs {
			sendReq := &calculatorpb.FindMaximumRequest{
				Number: req,
			}
			fmt.Printf("Send to Server: %v\n", req)
			stream.Send(sendReq)
			time.Sleep(100 * time.Millisecond)
		}
		stream.CloseSend()
	}()

	go func() {
		for {
			mes, err := stream.Recv()
			if err == io.EOF {
				break
			}
			if err != nil {
				log.Fatalf("error when receiving stream from server: %v", err)
			}

			fmt.Printf("max elemnet: %v\n", mes.GetMax())
		}
		close(waitc)
	}()

	<-waitc
}

func doErrorUnary(c calculatorpb.CalculateServiceClient) {
	fmt.Println("Starting to do SquareRoot unary RPC...")

	doErrorCall(c, 10)
	doErrorCall(c, -2)
}

func doErrorCall(c calculatorpb.CalculateServiceClient, n int32) {

	req := &calculatorpb.SquareRootRequest{
		Number: n,
	}
	res, err := c.SquareRoot(context.Background(), req)

	if err != nil {
		resErr, ok := status.FromError(err)
		if ok {
			fmt.Println(resErr.Message())
		} else {
			log.Fatalf("Big Error calling SquareRoot: %v", err)
		}
		return
	}

	fmt.Printf("Result of square root of %v: %v", n, res.GetNumberRoot())
}
