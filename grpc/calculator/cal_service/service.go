package main

import (
	"context"
	"fmt"
	"io"
	"log"
	"math"
	"net"
	"time"

	"gitlab.com/hoangvy/vng_research/grpc/calculator/calculatorpb"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type server struct{}

func (s *server) SumTwoNumber(ctx context.Context, req *calculatorpb.CalRequest) (*calculatorpb.CalResponse, error) {
	sum := req.GetArg1() + req.GetArg2()
	res := &calculatorpb.CalResponse{
		Res: sum,
	}
	return res, nil
}

func (s *server) PrimeNumberDecomposition(req *calculatorpb.PrimeNumberRequest, stream calculatorpb.CalculateService_PrimeNumberDecompositionServer) error {
	input := req.GetInput()

	k := int64(2)
	for input > 1 {
		if input%k == 0 {
			res := &calculatorpb.PrimeNumberResponse{
				Prime: k,
			}
			stream.Send(res)
			input = input / k
		} else {
			k = k + 1
		}
		time.Sleep(1000 * time.Millisecond)
	}
	return nil
}

func (s *server) ComputeAverage(stream calculatorpb.CalculateService_ComputeAverageServer) error {
	total, count := int64(0), float32(0)

	for {
		mes, err := stream.Recv()
		if err == io.EOF {
			res := &calculatorpb.ComputeAverageRespone{
				Result: float32(total) / count,
			}
			return stream.SendAndClose(res)
			break
		}
		if err != nil {
			fmt.Printf("Error when reading stream: %v", err)
			break
		}

		total += mes.GetElement()
		count++
		fmt.Printf("Calculate total from client: %v, %v \n", total, count)
	}
	return nil
}

func (s *server) FindMaximum(stream calculatorpb.CalculateService_FindMaximumServer) error {
	max := int64(0)

	for {
		//listen stream
		mes, err := stream.Recv()

		if err == io.EOF {
			return nil
		}

		if err != nil {
			log.Fatalf("Error when reading stream: %v", err)
		}

		if mes.GetNumber() > max {
			max = mes.GetNumber()
		}
		fmt.Printf("calculate for: %v", mes.GetNumber())
		res := &calculatorpb.FindMaximumResponse{
			Max: max,
		}
		sendErr := stream.Send(res)
		if sendErr != nil {
			log.Fatalf("error when send response: %v", err)
		}
	}
}

func (s *server) SquareRoot(ctx context.Context, req *calculatorpb.SquareRootRequest) (*calculatorpb.SquareRootResponse, error) {
	number := req.GetNumber()

	fmt.Printf("numner: %d", number)
	if number < 0 {
		return nil, status.Errorf(codes.InvalidArgument, fmt.Sprintf("Received a negative number: %v", number))
	}

	return &calculatorpb.SquareRootResponse{
		NumberRoot: math.Sqrt(float64(number)),
	}, nil

}

func main() {
	ls, err := net.Listen("tcp", ":50052")
	if err != nil {
		log.Fatal("Can not listen to post 50052")
	}

	s := grpc.NewServer()
	calculatorpb.RegisterCalculateServiceServer(s, &server{})

	if err := s.Serve(ls); err != nil {
		log.Fatal("Fail to connect to server, %v", err)
	}

}
