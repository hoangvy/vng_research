module gitlab.com/hoangvy/vng_research/grpc/grpc_stream/client

go 1.14

require (
	gitlab.com/hoangvy/vng_research v0.0.0-20200320100006-4f34d52416a6
	google.golang.org/grpc v1.35.0
)
