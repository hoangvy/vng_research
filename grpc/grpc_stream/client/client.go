package main

import (
	"context"
	"fmt"
	"gitlab.com/hoangvy/vng_research/grpc/calculator/calculatorpb"
	streampb "gitlab.com/hoangvy/vng_research/grpc/grpc_stream/proto"
	"google.golang.org/grpc"
	"io"
	"log"
)

func main() {
	conn, err := grpc.Dial("localhost:50052", grpc.WithInsecure())
	if err != nil {
		log.Fatal("Can not connect to port, %v", err)
	}

	defer conn.Close()

	c := calculatorpb.NewCalculateServiceClient(conn)

	doServerStream(c)
}

func doServerStream(client streampb.StreamServiceClient) {
	fmt.Println("Starting Server Streaming RPC...")
	req := &streampb.FilterRequest{
		Key: "Settings",
	}

	streamRes, err := client.FilterData(context.Background(), req)
	if err != nil {
		log.Fatalf("error when calling Prime: %v", err)
	}

	for {
		msg, err := streamRes.Recv()
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Fatalf("error when reading stream: %v", err)
		}
		fmt.Printf("Prime return from server: %v \n ", msg.GetResponse())
	}
}