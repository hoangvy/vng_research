package main

type Data struct {
	Title string `json:"title"`
	Children []Data `json:"children,omitempty"`
}

func filterKey(key string, in []Data) (out []Data) {
	if key == "" {
		return []Data{}
	}

	for _, d := range in {
		if d.Title == key {
			out = append(out, d)
		} else {
			filterChild := filterKey(key, d.Children)
			if len(filterChild) != 0 {
				d.Children = filterChild
				out = append(out, d)
			}
		}
	}

	return out
}