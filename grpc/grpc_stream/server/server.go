package main

import (
	"encoding/json"
	streampb "server/streampb/proto"
	"google.golang.org/grpc"
	"log"
	"net"
	"time"
)

type server struct{}

func main() {
	ls, err := net.Listen("tcp", ":50052")
	if err != nil {
		log.Fatal("Can not listen to post 50052")
	}

	s := grpc.NewServer()
	streampb.RegisterStreamServiceServer(s, &server{})

	if err := s.Serve(ls); err != nil {
		log.Fatal("Fail to connect to server, %v", err)
	}

}

func (s *server) FilterData(req *streampb.FilterRequest, stream streampb.StreamService_FilterDataServer) error {
	key := req.GetKey()

	time.Sleep(1000 * time.Millisecond)
	out := filterKey(key, ConvertData([]byte(customData)))
	outString, _ := json.Marshal(out)
	stream.Send(&streampb.FilterResponse{
		Response: string(outString),
	})

	return nil
}
