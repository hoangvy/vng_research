package main

import "encoding/json"

const customData = `
	[
   {
      "title":"Trading",
      "children":[
         {
            "title":"Customers",
            "children":[
               {
                  "title":"Orders"
               },
               {
                  "title":"Trades"
               }
            ]
         },
         {
            "title":"Suppliers",
            "children":[
               {
                  "title":"Orders"
               },
               {
                  "title":"Trades"
               }
            ]
         }
      ]
   },
   {
      "title":"General",
      "children":[
         {
            "title":"Customers",
            "children":[
               {
                  "title":"Settings"
               },
               {
                  "title":"Access Control"
               }
            ]
         },
         {
            "title":"Suppliers",
            "children":[
               {
                  "title":"Settings"
               },
               {
                  "title":"Access Control"
               }
            ]
         }
      ]
   }
]
`

func ConvertData(in []byte) []Data {
	data := []Data{}

	err := json.Unmarshal(in, &data)
	if err != nil {
	return []Data{}
	}

	return data
}