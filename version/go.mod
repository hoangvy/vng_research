module gitlab.com/hoangvy/vng_research/version

go 1.14

require (
	gitlab.com/hoangvy/ca_verion_not_module v1.0.0 // indirect
	gitlab.com/hoangvy/cal_version v1.0.1
)
