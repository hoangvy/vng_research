package main

import (
	"fmt"

	calc "gitlab.com/hoangvy/cal_version"
)

func main() {
	addition := calc.Add(1, 2)
	fmt.Println(addition)
}
