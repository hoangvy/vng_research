package main

import (
	"encoding/json"
	"log"
	"net/http"
	"strings"
)

var messageChannels = make(map[chan []byte]bool)

func formatSSE(event string, data string) []byte {
	eventPayload := "event" + event + "\n"
	dataLines := strings.Split(data, "\n")

	for _, line := range dataLines {
		eventPayload = eventPayload + "data: " + line + "\n"
	}

	return []byte(eventPayload + "\n")
}

func sayHandler(w http.ResponseWriter, r *http.Request) {
	name := r.FormValue("name")
	message := r.FormValue("message")

	jsonStructure, _ := json.Marshal(map[string]string{
		"name":    name,
		"message": message,
	})

	go func() {
		for messageChannel := range messageChannels {
			log.Printf("channel: %v", messageChannel)
			messageChannel <- []byte(jsonStructure)
		}
	}()
}

func listenHandler(w http.ResponseWriter, r *http.Request) {
	_messageChannel := make(chan []byte)
	messageChannels[_messageChannel] = true

	w.Header().Set("Connection", "keep-alive")
	w.Header().Set("Content-Type", "text/event-stream")
	w.Header().Set("Access-Control-Allow-Origin", "*")

	for {
		select {
		case _msg := <-_messageChannel:
			w.Write(formatSSE("message", string(_msg)))
			log.Println("receive message")
			w.(http.Flusher).Flush()
		case <-r.Context().Done():
			log.Printf("Done listen: %v", messageChannels)
			delete(messageChannels, _messageChannel)
			log.Printf("After listen: %v", messageChannels)
			return
		}
	}

}

func main() {
	http.HandleFunc("/say", sayHandler)
	http.HandleFunc("/listen", listenHandler)
	log.Fatal(http.ListenAndServe(":4000", nil))
}
